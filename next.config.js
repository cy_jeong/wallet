/** @type {import('next').NextConfig} */

const path = require('path');
const withPWA = require("@ducanh2912/next-pwa").default({
  dest: "public",
  workboxOptions: {
    disableDevLogs: true
  },
  eslint: {
    ignoreDuringBuilds: true
  },
  api: {
    requestLimit: false,
    responseLimit: false,
    bodyParser: {
      sizeLimit: '10mb'
    }
  },
  experimental: {
    serverActions: {
      bodySizeLimit: '10mb' // Set desired value here
    }
  }
});

const nextConfig = {
  reactStrictMode: false,
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')]
  }
}
module.exports = withPWA(nextConfig);
