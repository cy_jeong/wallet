'use client'

import { ComponentWrap } from '@/src/component/layout/theme';
import ListPage from '@/src/component/List/list';

export default function List() {
  return (
    <ComponentWrap>
      <ListPage/>
    </ComponentWrap>
  )
}