import type { Metadata } from "next";
import "./globals.css";
import RecoilProvider from "./RecoilProvider";

export const metadata: Metadata = {
  manifest: '/manifest.json',
  title: 'MH',
  description: 'My home',
  themeColor: '#FFFFFF',
  icons: '/favicon.ico',
  viewport: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="ko">
      <body style={{margin: 0, padding: 0}}>
        <RecoilProvider>
          {children}
        </RecoilProvider>
      </body>
    </html>
  );
}
