'use client';

import {ReactElement, ReactNode, useEffect} from 'react';
import {RecoilRoot} from 'recoil';

interface IProps
{
  children?: ReactNode | undefined
}

function RecoilProvider({children}: IProps): ReactElement {
  // useEffect(() => {
  //   window.addEventListener('mousemove', () => {
  //     console.log('mousemove');
  //   })

  //   window.addEventListener('touchmove', () => {
  //     console.log('touchmove');
  //   })

  //   return () => {
  //     window.removeEventListener('mousemove', () => {

  //     })
  //   }
  // }, [])

  return <RecoilRoot>{children}</RecoilRoot>;
}

export default RecoilProvider;
