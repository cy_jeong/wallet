'use client'

import { ComponentWrap } from "@/src/component/layout/theme"
import LoginForm from "@/src/component/login/login"
import { Viewport } from "next"

export const viewport: Viewport = {
  themeColor: '#FFFFFF'
}

export default async function Login() {
  // console.log('LoginFrom : ', LoginForm);
  return (
    <>
      <ComponentWrap>
        <LoginForm />
      </ComponentWrap>
    </>
  )
}