'use client'

import {ComponentWrap} from "@/src/component/layout/theme"
import PDFForm from "@/src/component/sample/pdf";
import SampleForm from "@/src/component/sample/sample"

interface IProps {
  params: {
    id: string;
  };
}

export default async function sample({params}: IProps) {
  console.log('id : ', params.id)
  return (
    <>
      <ComponentWrap>
        {/* <SampleForm /> */}
        <PDFForm />
        {/* {params.id === '1' && {
          <SampleForm />
        }} */}
        
      </ComponentWrap>
    </>
  )
}