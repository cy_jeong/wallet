import dayjs from "dayjs";
import { NextRequest, NextResponse } from "next/server";

const logTime = `[${dayjs().format('YYYY-MM-DD HH:mm:ss')}]`;

export default async function middleware(req: NextRequest) {
  const response = NextResponse.next()

  const {pathname} = req.nextUrl

  console.log(`${logTime} - pathName : ${pathname}`)

  if (pathname.startsWith('/') && pathname.length === 1) {
    const url = req.nextUrl.clone();
    url.pathname = '/login';
    return NextResponse.redirect(url);
  }

  return response;
}

// See "Matching Paths" below to learn more
// 불필요하게 middleware를 탈 필요 없는것들을 설정해 놓는 곳
export const config = {
  matcher: [
    /*
     * api (API 라우트)
     * optimization
     * _next/static (정적 파일)
     * _next/image (이미지 최적화 파일)
     * favicon.ico (파비콘 파일)
     * 로 시작하지 않는 모든 요청 경로와 일치합니다.
     */
    '/((?!api|v1|optimization/get_weight|_next/static|_next/image|favicon.ico).*)'
  ]
};