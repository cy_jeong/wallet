import {atom} from 'recoil';
import { IBeforeInstallPromptEvent } from '@/src/types/prompt';

export const promptInstallState = atom<boolean>({
  key: 'PROMPT_INSTALL_EVENT_STATE',
  default: false
});

export const promptEventState = atom<IBeforeInstallPromptEvent | undefined>({
  key: 'PROMPT_EVENT_STATE',
  default: undefined
});
