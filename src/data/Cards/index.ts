import {atom} from 'recoil';
import {ICards} from '@/src/types/member';

export interface ICardList {
  cards: ICards[];
  visible: boolean;
}

export const cardsState = atom<ICardList>({
  key: 'CARDS_STATE',
  default: undefined
});
