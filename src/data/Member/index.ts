import {atom} from 'recoil';
import { IMember } from '@/src/types/member';

export const memberState = atom<IMember>({
  key: 'MEMBER_STATE',
  default: undefined
});
