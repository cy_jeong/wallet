import {atom} from 'recoil';
import {ReactElement} from 'react';

export interface IAlertModal {
  title?: string;
  content: string | ReactElement | ReactElement[];
  size?: 'lg' | 'md' | 'sx';
  confirmLabel?: string;
  cancelLabel?: string;
  hasCancelButton?: boolean;
  hasClose?: boolean;
  onConfirm?(): void;
  onCancel?(): void;
}

export const alertModalState = atom<IAlertModal | undefined>({
  key: 'ALERT_MODAL_STATE',
  default: undefined
});
