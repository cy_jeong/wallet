import {atom} from 'recoil';

type RowProps = {
  left: string;
  right: string;
};

interface Props {
  title?: string;
  content: string;
  info?: Array<RowProps>;
  textConfirm?: string;
  onConfirm?: () => void;
}

export const stateAlertMessage = atom<Props | undefined>({
  key: 'STATE_ALERT_MESSAGE',
  default: undefined
});
