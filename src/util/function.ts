export function isEmptyString(value: string | undefined | null) {
  if (value === null || value == undefined)
    return true;

  return value.length <= 0;
}

export function isEmptyObject(value: Object | any) {
  return value === null || value === undefined;
}

export function isEmptyArray(value: Object) {
  if (isEmptyObject(value))
    return true;

  if (Array.isArray(value) && value.length > 0)
    return false;

  return true;
}

export const isStandalone = typeof window !== 'undefined' && window.matchMedia('(display-mode: standalone)').matches;