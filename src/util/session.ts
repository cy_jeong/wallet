export function setCookie(key: string, value: string) {
  localStorage.setItem(key, value);
}

export function getCookie(key: string): string | null{
  return localStorage.getItem(key);
}