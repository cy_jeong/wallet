import { MongoClient, MongoClientOptions } from 'mongodb'
const url = 'mongodb://letshome.synology.me:2717'
const options: MongoClientOptions = { }
let mongoDB: Promise<MongoClient>

declare global {
  var _mongo: Promise<MongoClient> | undefined;
}

// if (process.env.NODE_ENV === 'development') {
//   if (!global._mongo) {
//     global._mongo = new MongoClient(url, options).connect()
//   }
  
//   mongoDB = global._mongo
//} else {
  mongoDB = new MongoClient(url, options).connect()  
// }


export { mongoDB }