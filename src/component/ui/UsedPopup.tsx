import { Modal } from "@material-ui/core"
import { Box, Button, Checkbox, FormControlLabel, TextField, Typography } from "@mui/material"
import { Col, Row } from "../layout/theme";
import { ChangeEvent, RefObject, useEffect, useRef, useState } from "react";
import 'dayjs/locale/ko'
import dayjs from "dayjs";
import { createTheme } from '@mui/material/styles'
import css from "styled-jsx/css";
import { IImageInfo, IUsedData } from "@/src/types/member";
import { isEmptyString } from "@/src/util/function";
// import { CSSProperties } from "styled-components";
import ImageViewer from "./ImageViewer";
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

interface IProps {
  usedData: IUsedData;
  onSave(usedData: IUsedData): void;
  onClose(): void;
}

const myTheme = createTheme({
  // Set up your custom MUI theme here
})

function UsedPopup({
  usedData,
  onSave,
  onClose
}: IProps) {
  const [used, setUsed] = useState<IUsedData>(usedData);
  const [selectDate, setSelectDate] = useState(new Date(usedData?.id));
  const [imageInfo, setImageInfo] = useState<IImageInfo|undefined>(undefined)
  // const canvasRef: RefObject<HTMLCanvasElement> = useRef<HTMLCanvasElement>(null)

  const onPriceChange = (e: ChangeEvent<HTMLInputElement>) => {
    const {value} = e.target;
    setUsed((prev) => ({...prev, price: value.replaceAll(',', '')}))
  }

  const onInsuranceChange = (e: ChangeEvent<HTMLInputElement>) => {
    const {value} = e.target;
    setUsed((prev) => ({...prev, insuranceBack: value.replaceAll(',', '')}))
  }

  const onMemoChange = (e: ChangeEvent<HTMLInputElement>) => {
    setUsed((prev) => ({...prev, memo: e.target.value}))
  }

  const handleImageChange = (e: any) => {
    if (e.target.files && e.target.files.length > 0) {
      const file = e.target.files[0];
      const reader = new FileReader();

      reader.onloadend = () => {
        const img = new Image()
          img.src = reader.result as string
          img.onload = function(): void {
            setUsed((prev) => ({...prev, image: {
              source: reader.result as string,
              width: img.width,
              height: img.height
            }})) 
          }
      };
      
      reader.readAsDataURL(file);
    }
  };

  const handleSaveClick = (saveType: 'in' | 'out' | 'update') => {
    fetch('/api/data/list', {
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      },
      body: saveType === 'update' ?
        JSON.stringify(used)
        :
        JSON.stringify({
        ...used,
        _id: dayjs().valueOf().toString(),
        inOut: saveType === 'in' ? true : false
      })
    })
    .then((json) => json.json())
    .then((json) => {
      if (json.success)
        onSave(json.data as IUsedData)
      else
        console.log('error : ', json)
    })
  }

  const onChangeDate = (date: Date | null) => {
    if (date) {
      setSelectDate(date)
      setUsed((prev) => ({...prev, id: Number(dayjs(date))}))
    }
  }

  useEffect(() => {
    dayjs.locale('ko')
  }, []);
  
  return (
    <>
      <Modal className="alert-modal-module" open onClose={undefined} style={{zIndex: 1}}>
        <Col
          sx={{
            bgcolor: 'white',
            height: '80%',
            bottom: '0px',
            borderRadius: '10px',
            position: 'fixed',
            left: '6px',
            width: 'calc(100% - 12px)'
          }}>
          {/* <Row sx={{justifyContent: 'space-between'}}> */}
          <Row sx={{height: '40px', width: '100%', alignItems: 'center'}}>
            <Box sx={{width: '100%', textAlign: 'center'}}>
              <DatePicker
                selected={selectDate}
                onChange={onChangeDate}
                dateFormat="yyyy-MM-dd"
                className="react-datepicker-input"
              />
            </Box>
            <Button style={{position: 'fixed', right: '5px'}} onClick={() => onClose()}>닫기</Button>
          </Row>
          <Col style={{gap: '5px'}}>
            <Row>
              {/* <Row className="label-title">
                <input
                  type="checkbox"
                  id="shareCheck"
                  checked={used.share}
                  onChange={(e) => {
                  setUsed((prev) => ({...prev, share: e.target.checked}))
                }}
                />
                <label
                  htmlFor="shareCheck"
                  style={{width: '100%', alignContent: 'center'}}
                >금액</label>
              </Row> */}
              <Row className="label-title">
              <FormControlLabel
                label="금액"
                sx={{width: '100%'}}
                control={<Checkbox checked={used.share}
                onChange={(e) => setUsed((prev) => ({...prev, share: e.target.checked}))}/>}
              />
              </Row>
              <Row sx={{width: '100%'}}>
                <TextField
                  sx={{width: '100%', mr: '5px'}}
                  className="numberText"
                  InputProps={{
                    inputProps: {
                      maxLength: 15,
                      inputMode: 'numeric',
                      pattern: '[0-9]*'
                    }
                  }}
                  type='text'
                  value={used.price.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                  onChange={onPriceChange}
                />
              </Row>
            </Row>
            <Row sx={{height: '100px'}}>
              <label className="label-title">메모</label>
              <TextField
                sx={{width: '100%', mr: '5px'}}
                className="multilineText"
                multiline
                maxRows={3}
                value={used.memo}
                onChange={onMemoChange}
              />
            </Row>
            <Row>
              <label className="label-title">보험 청구</label>
              <TextField
                sx={{width: '100%', mr: '5px'}}
                className="numberText"
                InputProps={{
                  inputProps: {
                    maxLength: 15,
                    inputMode: 'numeric',
                    pattern: '[0-9]*'
                  }
                }}
                type='text'
                value={used.insuranceBack ? used.insuranceBack.replace(/\B(?=(\d{3})+(?!\d))/g, ",") : ''}
                onChange={onInsuranceChange}
              />
            </Row>
            <Row>
              <label className="label-title">이미지</label>
              <Row sx={{width: '100%'}}>
                <label
                  htmlFor="input-file"
                  style={{
                    border: '1px solid darkgray',
                    borderRadius: '5px',
                    width: '70%',
                    textAlign: 'center',
                    alignContent: 'center'
                  }}
                >
                  불러오기
                </label>
                <input type="file" accept="image/*" id="input-file" style={{display: 'none'}} onChange={handleImageChange} />
                <Button
                  sx={{ml: '5px', padding: '5px'}}
                  variant="contained"
                  className="multilineText"
                  disabled={used.image === undefined}
                  onClick={() => {setImageInfo(used.image)}}
                >
                  미리보기
                </Button>
              </Row>
            </Row>
            <Row
              sx={{
                width: '100%',
                position: 'fixed',
                bottom: '10px',
                justifyContent: 'space-around'
                }}>
              {isEmptyString(used._id) ? (
                <>
                  <Button sx={{bgcolor: '#394add', color: "#fff", textAlign:"center"}} variant="outlined" onClick={() => handleSaveClick('out')}>
                    <Typography variant="h6">출금</Typography>
                  </Button>
                  <Button sx={{bgcolor: '#f53b52', color: "#fff", textAlign:"center"}} variant="outlined" onClick={() => handleSaveClick('in')}>
                    <Typography variant="h6">입금</Typography>
                  </Button>
                </>
              ) : (
                <Button sx={{bgcolor: '#394add', textAlign:"center", color:"#fff"}} variant="outlined" onClick={() => handleSaveClick('update')}>
                  <Typography variant="h6" >저장</Typography>
                </Button>
              )}
              <Button sx={{bgcolor: '#394add', textAlign:"center", color:"#fff"}} variant="outlined" onClick={onClose}>
                <Typography variant="h6">취소</Typography>
              </Button>
            </Row>
          </Col>
        </Col>
      </Modal>
      {imageInfo &&
        <ImageViewer
          source={imageInfo.source as string}
          width={imageInfo.width}
          height={imageInfo.height}
          onClose={() =>setImageInfo(undefined)}
        />
      }
      {/* <style jsx>{style}</style> */}
    </>
  )
}

export default UsedPopup