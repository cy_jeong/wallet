'use client'

import { ReactElement, useEffect, useState } from "react"
import css from 'styled-jsx/css';

const style = css`
  .overlay {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.5);
    display: flex;
    justify-content: center;
    align-items: center;
    z-index: 999;
  }

  .popup {
    background-color: white;
    padding: 25px;
    border-radius: 5px;
    box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
    position: relative;
    min-width: 300px;
  }

  .closeButton {
    position: absolute;
    top: 5px;
    right: 5px;
    background: none;
    border: none;
    font-size: 1.2em;
    cursor: pointer;
  }
`;

interface IProps {
  source: string;
  width: number;
  height: number;
  onClose(): void;
}

function ImageViewer({source, width, height, onClose}: IProps): ReactElement {
  const [client, setClient] = useState<any>({w: 0, h: 0})
  const [canvas, setCanvas] = useState<HTMLCanvasElement | null>(null);
  
  useEffect(() => {
    const popupElement = document.getElementById("popupElement")
    setClient({w: Number(popupElement?.clientWidth) - 50, h: Number(popupElement?.clientHeight) - 50})

    const element = document.getElementById("myCanvas")
    if (element) setCanvas(element as HTMLCanvasElement);
  }, [])

  useEffect(() => {
    if (canvas) {
      const context = canvas.getContext('2d')
      if (context) {
        const img = new Image()
          img.src = source
  
          img.onload = function(): void {
            // const revision = Math.min(client.w, client.h) / Math.max(width, height)
            // const revisionW = width * revision;
            // const revisionH = height * revision;

            const revisionW = (client.w / width) * width
            const revisionH = (client.h / height) * height

            // console.log('client : ', client)
            // console.log('revision : ', revision)
            // console.log('revisionW : ', revisionW)
            // console.log('revisionH : ', revisionH)
            
            context.drawImage(img, 0, 0, revisionW, revisionH)
          }
      }
    }
  }, [canvas])

  return (
    <>
      <div className="overlay">
        <div className="popup" id="popupElement" style={{width: '90%', height: '90%'}}>
          <button className="closeButton" onClick={onClose}>X</button>
          <canvas
            id="myCanvas"
            width={client.w}
            height={client.h}
            style={{border: '1px solid #000000'}}
          />
        </div>
        <style jsx>{style}</style>
      </div>
    </>
  )
}

export default ImageViewer;