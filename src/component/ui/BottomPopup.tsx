import { FormControlLabel, InputLabel, MenuItem, Modal, Radio, RadioGroup, Select } from "@material-ui/core"
import { Button, TextField } from "@mui/material"
import { Col, Row } from "../layout/theme";
import { LocalizationProvider, DesktopDatePicker } from "@mui/x-date-pickers";
import { ChangeEvent, useState } from "react";
import 'dayjs/locale/ko'
import dayjs, { Dayjs } from "dayjs";
import {AdapterDayjs} from '@mui/x-date-pickers/AdapterDayjs';
import { createTheme, ThemeProvider } from '@mui/material/styles'
import MUIRichTextEditor from "mui-rte";
import css from "styled-jsx/css";

interface IProps {
  onSave?(): void;
  onClose?(): void;
}

const myTheme = createTheme({
  // Set up your custom MUI theme here
})

const style = css`
  .back {
    /* width: 100%;
    height: 100%; */
    /* background-color: transparent; */

    .frame {
      width: 90%;
      left: 5%;
      min-height: 300px;

      bottom: 0px;
      position: fixed;

      /* background-color: rgba(0, 0, 0, 1); */
      /* background-color: transparent; */
      background-color: #ffffff;
        opacity: 0.5;

      .buttons {
      gap: 10px;
      display: flex;

      position: fixed;
    }
    }
  }
`

function BottomPopup({
  onSave,
  onClose
}: IProps) {
  const [dpValue, setValue] = useState<Dayjs | null>(dayjs);
  const [price, setPrice] = useState<string>('');
  const [useMemo, setUseMemo] = useState<string>('');

  const onPriceChange = (e: ChangeEvent<HTMLInputElement>) => {
    const {value} = e.target;
    setPrice(value.replaceAll(',', ''));
  }

  const onMemoChange = (e: ChangeEvent<HTMLInputElement>) => {
    const {value} = e.target;
    setUseMemo(value);
  }
  
  return (
    <>
      <div className="back">
        <div className="frame">
          <div className="buttons">
            <Button onClick={onSave}>확인</Button>
            <Button onClick={onClose}>취소</Button>
          </div>
        </div>
      </div>
      <style jsx>{style}</style>
    </>
  )
}

export default BottomPopup