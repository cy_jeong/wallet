'use client'

import { useEffect, useState, ReactElement } from 'react';
import PageContainer from "../layout/PageContainer";
import dayjs from 'dayjs';

function SampleForm(): ReactElement {
  const [text, setText] = useState<string>('');
  
  useEffect(() => {
    // try {
    //   const date = dayjs('2024-06-22 00:00:00').format('YYYY-MM-DD HH:mm:ss')
    //   console.log('date : ', date)
    //   setText(date as string);
    // } catch (ex) {
    //   console.log('error : ', ex)
    //   // setText(ex)
    // }

    setText(dayjs(dayjs().valueOf()).format('YYYY-MM-DD HH:mm:ss'))
  }, [])

  return (
    <>
      <PageContainer>
        Sample Page
        <p>
          {text}
        </p>
      </PageContainer>
    </>
  )
}

export default SampleForm;