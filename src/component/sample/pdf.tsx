'use client'

import { useEffect, useState, ReactElement } from 'react';
import PageContainer from "../layout/PageContainer";
import dayjs from 'dayjs';
import { Button } from '@mui/material';

function PDFForm(): ReactElement {
  const [text, setText] = useState<string>('');
  const htmlCode = `<!DOCTYPE html>
  <html lang="en">
    <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>
        금융거래정보 등의 제공사실 통지 동의서 (KB증권 오픈API 서비스용)
      </title>
  
      <style>
          @font-face{font-family:"webisfree";src:local("webisfree"),url("./fonts/webisfree.ttf")}body{margin:0;padding:0;counter-reset:list-item;font-family:"Noto Sans KR","Apple SD Gothic Neo";border:.625em solid #fc0;padding:2em;font-size:.7rem}h1{margin-top:0;font-size:1.3rem}p{margin:0;margin-bottom:16px;line-height:1.5}.tout{border:.06em solid #ffc000;width:100%}.cout{border-bottom:.06em solid #ffc000;padding:.625em}.simg{height:1em}.t1{padding:0px .31em;background-color:#545045;display:inline-block;line-height:2.45em;text-align:center;height:2.45em;border-radius:1.25em;color:#fff}.st1{background-color:#fff;display:inline-block;line-height:1.8em;text-align:center;width:1.8em;height:1.8em;border-radius:.95em;color:#000}.st2{display:inline-block;padding:0px .31em;font-size:1em}.bgy{width:30%;background-color:#fc0;padding:.5%;font-size:.7rem;font-weight:600}.bgg{background-color:#d3d3d3}.tright{text-align:right;padding:.625em}.tcenter{text-align:left;padding:.625em}.tcenter img{width:20%}@media only screen and (min-width: 200px)and (max-width: 480px){.tcenter img{width:35%}}.tleft{text-align:left}.bxy{display:inline-block;padding:.625em .125em;background-color:#ffc000}.bxg{display:inline-block;padding:.625em;background-color:#e1dfdf}ul{list-style:none;padding-left:0em}li::before{content:"- "}
      </style>
    </head>
    <body>
      <div class="out">
        <h1>금융거래정보 등의 제공사실 통지 동의서 (KB증권 오픈API 서비스용)</h1>
  
        <p>
          <b>KB증권 귀중</b>
        </p>
        <p>
          KB증권은 오픈 API 서비스를 제공하기 위하여 고객님의 신용정보 및
          금융거래에 관한 정보 등을 서비스 제공자 에게 제공한 경우에는 이에 관하여
          『금융실명거래 및 비밀보장에 관한 법률』 제 4조의 2에 따라 서비스
          제공자에 대한 금융거래정보 등 포괄적 정보제공 및 해당 정보제공 내역을
          6개월 단위로 일괄 통보하는 것에 대해서 본인의 동의가 필요합니다.
        </p>
  
        <table cellspacing="0" class="tout">
          <tr>
            <td class="bgy">통지할 정보 등의 내용</td>
            <td class="cout">
              <ul>
                <li>
                  KB증권 오픈API 서비스를 통해 KB증권에 요청한 본인의 금융거래
                  정보
                </li>
              </ul>
            </td>
          </tr>
          <tr>
            <td class="bgy">정보 등을 제공 및 사실통지 금융기관</td>
            <td class="cout">
              <ul>
                <li>KB증권</li>
              </ul>
            </td>
          </tr>
          <tr>
            <td class="bgy">거부 권리 및 불이익</td>
            <td class="cout">
              귀하는 동의를 거부하실 수 있습니다. 다만 위 금융거래정보 등의
              제공사실 통지 동의는 KB증권 오픈API를 이용하기 위해 필수적이므로, 위
              사항에 동의하셔야만 서비스를 이용하실 수 있습니다.
            </td>
          </tr>
          <tr>
            <td class="bgy">금융거래정보 등의 제공하실 통지 동의 여부</td>
            <td class="cout">
              귀사가 위와 같이 본인의 제공된 금융거래정보 등을 일괄 통보하는 것에
              동의하십니까?
            </td>
          </tr>
        </table>
        <br />
  
        <table cellspacing="0" width="100%">
          <tr>
            <td colspan="5" class="tcenter"><img src="./img/logo.png" /></td>
          </tr>
          <tr>
            <td width="10%" class="tright">년</td>
            <td width="10%" class="tright">월</td>
            <td width="10%" class="tright">일</td>
            <td width="35%" class="tleft"><span class="bgg">본인 성명</span>:</td>
            <td width="35%" class="tright">서명 또는 (인)</td>
          </tr>
        </table>
      </div>
    </body>
  </html>
  `
  
  useEffect(() => {

  }, [])

  const onDownload = () => {
    fetch('/convertPdf', {
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify({
        convertType: 'HTML',
        content: htmlCode,
        stampX: 10,
        stampY: 10,
        width: 50,
        height: 50
      })
    })
    .then((resp) => resp.json())
    .then((data) => {
      if (Number(data.responseCode) === 0) {
        const toPdf = new Blob([Buffer.from(data.content)], {type: 'application/pdf'});
        const download = window.URL.createObjectURL(toPdf as Blob);
        const link = document.createElement('a');
        link.href = download;
        link.setAttribute('download', `d:/일임 계약서.pdf`);
        document.body.appendChild(link);
        link.click();
        link.remove();
      }
    })
  }

  return (
    <>
      <PageContainer>
        <Button onClick={onDownload}>
          PDF TEST
        </Button>
      </PageContainer>
    </>
  )
}

export default PDFForm;