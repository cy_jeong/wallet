'use client';

import {ReactElement, useEffect} from 'react';
import {useRecoilState} from 'recoil';
import {Typography, Box, Modal, Button} from '@mui/material';
import {Col, Row} from '../layout/theme';
import {alertModalState, IAlertModal} from '@/src/data/AlertModal';
import '../../app/globals.css'
import css from 'styled-jsx/css';

const alertPage = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 'calc(100% - 48px)',
  maxWidth: '320px',
  backgroundColor: '#FFF',
  borderRadius: '8px'
}

const cancelButtonStyle = {
  borderRadius: '0 0 0 8px',
  py: '18px',
  bgcolor: '#f8f9fb',
  fontSize: '16px',
  color: '#7a7986'
};

const confirmButtonSyle = (borderRadius: string): object => {
  return {borderRadius, py: '18px', fontSize: '16px'};
};

function AlertModal(): ReactElement {
  const [alertModal, setAlertModal] = useRecoilState(alertModalState);

  useEffect(() => {
    const eventHandler = (e: CustomEvent) => {
      const opt: IAlertModal = e.detail;
      // 모달이 이미 떠 있다면 내용을 계속 추가 해줌
      if (alertModal) {
        opt.content = `${alertModal.content}\n${opt.content}\n\n`;
      }
      setAlertModal(opt);
    };
    document.addEventListener('modal', eventHandler as EventListener);

    return () => {
      document.removeEventListener('modal', eventHandler as EventListener);
    };
  }, [alertModal, setAlertModal]);

  const onClose = (): void => {
    setAlertModal(undefined);
  };

  const onClick = (): void => {
    alertModal?.onConfirm?.();
    setAlertModal(undefined);
  };

  const onCancel = (): void => {
    alertModal?.onCancel?.();
    setAlertModal(undefined);
  };

  if (alertModal === undefined) {
    return <></>;
  }

  return (
    <>
      <Modal open onClose={alertModal.hasClose ? onClose : undefined}>
        <Box sx={alertPage}>
          <Col sx={{p: '24px 20px', gap: '16px'}}>
            <Typography variant="h4" fontWeight={600} textAlign="center">
              {alertModal.title}
            </Typography>
            <Typography
              sx={{maxHeight: 'calc(100vh - 300px)', overflow: 'auto', whiteSpace: 'pre-line'}}
              textAlign="center"
            >
              {alertModal.content}
            </Typography>
          </Col>
          <Row>
            {alertModal.hasCancelButton && (
              <Button sx={cancelButtonStyle} onClick={onCancel}>
                {alertModal.cancelLabel || '취소'}
              </Button>
            )}
            <Button sx={confirmButtonSyle(alertModal.hasCancelButton ? '0 0 8px 0' : '0 0 8px 8px')} onClick={onClick}>
              {alertModal.confirmLabel || '확인'}
            </Button>
          </Row>
          {/* 버튼 1개 일때 */}
        </Box>
      </Modal>
    </>
  );
}

export default AlertModal;
