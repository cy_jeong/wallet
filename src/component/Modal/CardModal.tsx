'use client';

import {ReactElement, useEffect} from 'react';
import {useRecoilState} from 'recoil';
import {Typography, Box, Modal, Button} from '@mui/material';
import {Col, Row} from '../layout/theme';
import {alertModalState, IAlertModal} from '@/src/data/AlertModal';
import '../../app/globals.css'
import css from 'styled-jsx/css';
import { cardsState } from '@/src/data/Cards';

const alertPage = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 'calc(100% - 48px)',
  maxWidth: '320px',
  backgroundColor: '#FFF',
  borderRadius: '8px'
}

const cancelButtonStyle = {
  borderRadius: '0 0 0 8px',
  py: '18px',
  bgcolor: '#f8f9fb',
  fontSize: '16px',
  color: '#7a7986'
};

const confirmButtonSyle = (borderRadius: string): object => {
  return {borderRadius, py: '18px', fontSize: '16px'};
};

function CardModal(): ReactElement {
  const [cardModal, setCardModal] = useRecoilState(cardsState);

  useEffect(() => {
    const eventHandler = (e: CustomEvent) => {
      const opt: IAlertModal = e.detail;
      // 모달이 이미 떠 있다면 내용을 계속 추가 해줌
      // if (cardModal) {
      //   opt.content = `${cardModal.content}\n${opt.content}\n\n`;
      // }
      // setCardModal(opt);
    };
    document.addEventListener('modal', eventHandler as EventListener);

    return () => {
      document.removeEventListener('modal', eventHandler as EventListener);
    };
  }, [cardModal, setCardModal]);

  const onClose = (): void => {
    // setCardModal(undefined);
  };

  const onClick = (): void => {
    // cardModal?.onConfirm?.();
    // setCardModal(undefined);
  };

  const onCancel = (): void => {
    // cardModal?.onCancel?.();
    // setCardModal(undefined);
  };

  if (cardModal === undefined) {
    return <></>;
  }

  return (
    <>
      {/* <Modal open onClose={cardModal.visible ? onClose : undefined}>
        <Box sx={alertPage}>
          <Col sx={{p: '24px 20px', gap: '16px'}}>
            <Typography variant="h4" fontWeight={600} textAlign="center">
              {cardModal.title}
            </Typography>
            <Typography
              sx={{maxHeight: 'calc(100vh - 300px)', overflow: 'auto', whiteSpace: 'pre-line'}}
              textAlign="center"
            >
              {cardModal.content}
            </Typography>
          </Col>
          <Row>
            {cardModal.hasCancelButton && (
              <Button sx={cancelButtonStyle} onClick={onCancel}>
                {cardModal.cancelLabel || '취소'}
              </Button>
            )}
            <Button sx={confirmButtonSyle(cardModal.hasCancelButton ? '0 0 8px 0' : '0 0 8px 8px')} onClick={onClick}>
              {cardModal.confirmLabel || '확인'}
            </Button>
          </Row>
        </Box>
      </Modal> */}
    </>
  );
}

export default CardModal;
