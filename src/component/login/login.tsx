'use client'

import { IMember } from "@/src/types/member";
import { isEmptyString, isStandalone } from "@/src/util/function";
import { Button, TextField } from "@mui/material"
import { useEffect, useState, ReactElement } from 'react';
import AlertModal from "../Modal/AlertModal";
import { useRecoilState, useSetRecoilState } from "recoil";
import { alertModalState } from "@/src/data/AlertModal";
import PageContainer from "../layout/PageContainer";
import { useRouter } from "next/navigation";
import { memberState } from "@/src/data/Member";
import { getCookie, setCookie } from "@/src/util/session";
import { IBeforeInstallPromptEvent, beforeInstallPrompt } from "@/src/types/prompt";
import { promptEventState } from "@/src/data/Prompt";

const TextFieldStyle = {
  backgroundColor: '#b1b2b9',
  borderRadius: '5px',
  width: '100%'
}

function LoginForm(): ReactElement {
  const router = useRouter();
  const [userId, setUserId] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const setAlertModal = useSetRecoilState(alertModalState)
  const [member, setMember] = useRecoilState(memberState)
  const setPrompt = useSetRecoilState(promptEventState);

  const loginCheck = (id: string, pass: string, cookie: boolean) => {
    fetch(`/api/users/login?userId=${id}&password=${pass}`, {
      method: 'GET'
    })
    .then((rep) => rep.json())
    .then((rep) => {
      if (rep.error) {
        if (!cookie) {
          setAlertModal({content: rep.message})
        } else {
          setCookie('user', '')
        }
      } else {
        setMember(rep.user)
      }
    })
  }
  
  const btnClick = (e: any) => {
    e.preventDefault();
    if (isEmptyString(userId)) {
      setAlertModal({content: '아이디를 입력하세요'});
      return;
    }

    if (isEmptyString(password)) {
      setAlertModal({content: '비밀번호를 입력하세요'});
      return;
    }

    loginCheck(userId, password, false)
  }

  useEffect(() => {
    if (member) {
      setCookie('user', JSON.stringify(member))
      router.push('/list');
    }
  }, [member])

  useEffect(() => {
    if (isStandalone) {
      const user: IMember = JSON.parse(getCookie('user') as string);
      if (user) {
        setUserId(user.userId);
        setPassword(user.password);
        setMember(user);
      }
    }
  }, [])

  useEffect(() => {
    const eventHandler = (e: IBeforeInstallPromptEvent) => {
      e.preventDefault();
      setPrompt(e);
    };

    window.addEventListener(beforeInstallPrompt, eventHandler as any);

    return () => {
      window.removeEventListener(beforeInstallPrompt, eventHandler as any);
    };
  }, [setPrompt]);

  return (
    <PageContainer>
      <div className="content-wrapper">
        <div className="content-inner">
          <div className="content-header">로그인</div>
          <TextField
            sx={{...TextFieldStyle, marginBottom: '10px'}}
            label="아이디"
            value={userId}
            onChange={(e: any) => {setUserId(e.target.value)}}/>
          <br />
          <TextField
            sx={TextFieldStyle}
            type="password"
            label="비밀번호"
            value={password}
            onChange={(e: any) => {setPassword(e.target.value)}}
          />
          <br />
          <Button className="login-button" onClick={btnClick}>Login</Button>
        </div>
      </div>
      <AlertModal />
    </PageContainer>
  )
}

export default LoginForm;