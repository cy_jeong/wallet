import { TextField } from "@material-ui/core"
import {ReactElement, ReactNode} from 'react';

interface IProps {
  type?: 'number';
}

const CustomTextField = ({type = 'number'}: IProps) => {
  return <TextField size='medium'  />
}

export default CustomTextField