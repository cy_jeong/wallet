import clsx from 'clsx';
import {ReactElement, ReactNode} from 'react';
import css from 'styled-jsx/css';

const style = css`
  .view-container {
    display: flex;
    flex-direction: column;
    height: 100%;
    width: 100%;
    background-color: white;
  }
  .view-container.popup {
    position: fixed;
    top: 0;
    z-index: 1;
  }
`;

interface IProps {
  type?: 'default' | 'popup';
  children: ReactNode | ReactNode[];
}

function ViewContainer({type = 'default', children}: IProps): ReactElement {
  return (
    <div className={clsx('view-container', type)}>
      {children}
      <style jsx>{style}</style>
    </div>
  );
}

export default ViewContainer;
