'use client'
import { ReactElement, useCallback, useEffect, useState } from 'react';
import { useRecoilState } from 'recoil';
import { Button, Checkbox, FormControlLabel, MenuItem, Select, Typography } from '@mui/material';
import { Box } from '@material-ui/core';
import { DataGrid, GridColDef, GridRowId, GridRowSelectionModel, GridSlotsComponentsProps } from '@mui/x-data-grid';
import dayjs from 'dayjs';
import css from 'styled-jsx/css';

import { Col, Row } from '@/src/component/layout/theme';
import UsedPopup from '@/src/component/ui/UsedPopup';
import ImageViewer from '@/src/component/ui/ImageViewer';
import { memberState } from '@/src/data/Member';
import { IImageInfo, IMember, IUsedData } from '@/src/types/member';
import { isEmptyObject } from '@/src/util/function';
import { useRouter } from 'next/navigation';
import { getCookie, setCookie } from '@/src/util/session';
import styled from 'styled-components';
import { CheckBox } from '@mui/icons-material';

const style = css`
  .MuiDataGrid-main {
    .MuiDataGrid {
      .MuiDataGrid-row {
        .MuiDataGrid-cell {
          display: flex;
          width: 100%;
          justify-content: center;
        }
      }
    }
  }
`

export default function ListPage(): ReactElement {
  const router = useRouter()
  const [loading, setLoading] = useState(false)
  const [rows, setRows] = useState<IUsedData[]>([])
  const [filterRows, setFilterRows] = useState<IUsedData[]>([])
  const [member, setMember] = useRecoilState(memberState)
  const [currentDate, setCurrentDate] = useState(dayjs());
  const [selectedRows, setSelectedRows] = useState<GridRowId[]>([])
  const [imageInfo, setImageInfo] = useState<IImageInfo|undefined>(undefined)
  const [selectItem, setSelectItem] = useState<IUsedData|undefined>(undefined)
  const [inPrice, setInPrice] = useState<Number>(0)
  const [outPrice, setOutPrice] = useState<Number>(0)
  const [includeShare, setIncludeShare] = useState<boolean>(true);
  const [includeDelete, setIncludeDelete] = useState<boolean>(true);
  const [filterOption, setFilterOption] = useState<string>('1');
  const [headerPrice, setHeaderPrice] = useState<any>({in: 0, out: 0});

  //#region data
  const priceColor = (inOut: boolean) => {
    if (inOut) return 'red'
    
    return 'blue'
  }

  const inOutText = (inOut: boolean) => {
    if (inOut) return '입금'
    
    return '출금'
  }

  const onImageCellClick = (e: any) => {
    setImageInfo(e.value);
  }

  const columns: GridColDef[] = [
    {field: 'id'},
    {field: '_id'},
    {
      field: 'date',
      headerName: '사용일시',
      type: 'string',
      align: 'center',
      headerAlign: 'center',
      width: 100,
      renderCell: (params) => { 
        return <Button sx={{color: 'black'}} onClick={() => {onRowClick(params)}}>{dayjs(params.row?.id).format('YYYY-MM-DD')}</Button>
      }
    },
    {
      field: 'share',
      headerName: '공유',
      type: 'boolean',
      align: 'center',
      headerAlign: 'center',
      width: 20
    },
    {
      field: 'inOut',
      headerName: '입·출',
      align: 'center',
      headerAlign: 'center',
      width: 55,
      renderCell: (params) => { 
        return <div style={{color: priceColor(params.value)}}>{inOutText(params?.value)}</div>
      }
    },
    {
      field: 'price',
      // headerName: '금액',
      type: 'string',
      align: 'right',
      headerAlign: 'center',
      width: 130,
      display: 'flex',
      renderHeader: (params) => {
        if (filterOption === '1')
          return '금액'
        return (
          <Col sx={{textAlign: 'right'}}>
            <div style={{color: 'red'}}>{headerPrice.in.toLocaleString()}</div>
            <div style={{color: 'blue'}}>{headerPrice.out.toLocaleString()}</div>
          </Col>
        )
      },
      renderCell: (params) => {
        if (Number(params.row.insuranceBack) > 0) {
          return (
          <div>
            <Box sx={{color: priceColor(params.row?.inOut)}}>
              {params.row.price.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
            </Box>
            <div style={{color: 'red'}}>
              {`보험 지급 [${(params.row.insuranceBack.replace(/\B(?=(\d{3})+(?!\d))/g, ","))}]`}
            </div>
          </div>
        )}

        return <div style={{color: priceColor(params.row?.inOut)}}>
          {params.row.price.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
        </div>
      }
    },
    {
      field: 'writer',
      headerName: '사용자',
      align: 'center',
      headerAlign: 'center',
      width: 80,
    },
    {
      field: 'image',
      headerName: '이미지',
      headerAlign: 'center',
      align: 'center',
      type: 'string',
      width: 100,
      renderCell: (params) => {
        if (params.row.image)
          return (
            <Button onClick={() => {onImageCellClick(params)}}>
              <Typography sx={{textAlign: 'center', alignContent: 'center', width: '100%'}} >
                보기
              </Typography>
            </Button>
          )
        else
          return <div style={{textAlign: 'center'}}>None</div>
      }
    },
    {
      field: 'memo',
      headerName: '메모',
      align: 'left',
      headerAlign: 'center',
      width: 200
      // renderCell: (params) => {
      //   if (params.row.memo) {
      //     const memo = params.row.memo.replaceAll('/r', '<br />');
      //     console.log(params.row.memo.indexOf('/n'));

      //     return <>{memo}</>
      //   }
      // }
    },
    {
      field: 'insuranceBack',
      headerName: 'insuranceBack',
      width: 0
    }
  ];
  //#endregion data

  const calcPrice = (source: IUsedData[], filter: boolean) => {
    let input = 0;
    let output = 0;

    source.filter((item: IUsedData) => {
      if ((filter &&
          filterOption === '1' ||
          filterOption === '2' && item.inOut ||
          filterOption === '3' && !item.inOut ||
          filterOption === '4' && item.share) ||
          !filter
      ) {
        if (filter || (!filter && includeShare || !item.share)) {
          if (item.inOut) input += Number(item.price)
          else            output += Number(item.price)

          if (item.insuranceBack.length > 0)
            input += Number(item.insuranceBack)
        }
      }
    })

    if (filter) {
      setHeaderPrice({in: input, out: output})
    } else {
      setInPrice(input)
      setOutPrice(output)
    }
  }

  //#region component event
  const cbSaveClick = (usedData: IUsedData) => {
    setSelectItem(undefined)
    refresh();
  }

  const cbCloseClick = () => {
    setSelectItem(undefined)
  }

  const clickInsert = (e: any) => {
    e.preventDefault();

    setSelectItem({
      id: dayjs().valueOf(),
      share: false,
      inOut: true,
      price: '',
      memo: '',
      writer: member.name,
      userId: member.userId,
      insuranceBack: '',
      isNew: true
    })
  }

  const onRowClick = (e: any) => {
    setSelectItem(e.row)
  }

  const totalData = useCallback(() => {
    const from = Number(dayjs(currentDate.format('YYYYMM01000000')));
    const to = Number(dayjs(currentDate.format('YYYYMM31000000')));

    fetch(`/api/data/total?userId=${member?.userId}&fromDate=${from}&toDate=${to}&includeShare=${includeShare}`, {
      method: 'GET'
    })
    .then(json => json.json())
    .then(data => {
      setInPrice(data.total.inPrice || 0)
      setOutPrice(data.total.outPrice || 0)
    });
  }, [includeShare])

  const reloadData = useCallback(() => {
    const from = Number(dayjs(currentDate.format('YYYYMM01000000')));
    const to = Number(dayjs(currentDate.format('YYYYMM31000000')));

    fetch(`/api/data/list?userId=${member?.userId}&fromDate=${from}&toDate=${to}`, {
      method: 'GET'
    })
    .then(json => json.json())
    .then(data => {
      if (!data.error) {
        if (data.list) {
          const complete = data.list.filter((item: IUsedData) => !item?.complete)
          const deleted = complete.filter((item: IUsedData) => !item?.deleted)
          
          setRows(deleted)
          setFilterRows(deleted)

          calcPrice(deleted, false)

          setLoading(false);
        }
      }
    }
  )
  }, [rows, member, currentDate]);

  const deleteItem = () => {
    if (selectedRows.length <= 0)
      console.log('empty selected rows')
    else {
      setLoading(true);
      fetch(`/api/data/update`, {
        method: 'POST',
        headers: {
          'content-type': 'application/json'
        },
        body: JSON.stringify({id: selectedRows, deleted: true})
      })
      .then(json => json.json())
      .then(data => {
        setRows([]);
        reloadData();
      })
    }
  }

  const completeItem = () => {
    if (selectedRows.length <= 0)
      console.log('empty selected rows')
    else {
      setLoading(true);
      fetch(`/api/data/update`, {
        method: 'POST',
        headers: {
          'content-type': 'application/json'
        },
        body: JSON.stringify({id: selectedRows, complete: true})
      })
      .then(json => json.json())
      .then(data => {
        setRows([]);
        reloadData();
      })
    }
  }

  const refresh = () => {
    setRows([])

    setLoading(true);
    reloadData();
  }

  const prevMonth = () => {
    setCurrentDate(currentDate.subtract(1, 'M'))
  }

  const nextMonth = () => {
    setCurrentDate(currentDate.add(1, 'M'))
  }
  //#endregion component event

  //#region hook
  useEffect(() => {
    refresh();
  }, [currentDate])

  useEffect(() => {
    const cookie = getCookie('user');
    if (cookie) {
      const user: IMember = JSON.parse(cookie as string)
      fetch(`/api/users/login?userId=${user.userId}&password=${user.password}`, {
        method: 'GET'
      })
      .then((rep) => rep.json())
      .then((rep) => {
        if (rep.error) {
          setCookie('user', '');
          router.push('/login');
        } else {
          setMember(rep.user)
        }
      })
    } else {
      if (isEmptyObject(member)) {
        router.push('/login')
      }
    }
  }, [])

  useEffect(() => {
    if (member)
      reloadData();
  }, [member])

  useEffect(() => {
    // totalData();
    calcPrice(rows, false);
  }, [includeShare])

  useEffect(() => {
    if (rows.length > 0) {
      if (filterOption === '1') { // 전체
        setFilterRows(rows);
        calcPrice(rows, true)
      } else {
        const data = rows.filter(item => 
          filterOption === '2' && item.inOut ||
          filterOption === '3' && !item.inOut ||
          filterOption === '4' && item.share
        )
        setFilterRows(data);
        calcPrice(data, true)
      }
    }
  }, [filterOption])
  //#endregion hook

  return (
    <>
      <div className='content-wrapper'>
        <div className='content-inner' style={{overflow: 'auto'}}>
          <Col sx={{marginBottom: '7px'}}>
            <div className='content-header'>사용 내역</div>
            <Row sx={{justifyContent: 'space-between'}}>
              <Row>
                <button style={{border: 'none', backgroundColor: 'transparent', alignSelf: 'self-end'}} onClick={prevMonth}>
                  <svg width="7" height="16" viewBox="0 0 7 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 1.5L1 8L6 14.5" stroke="#201E33" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                  </svg>
                </button>
                <label style={{height: '100%', alignContent: 'center', padding: '0 5px'}}>
                  <Typography sx={{
                    color: '#201e33',
                    textAlign: 'center',
                    fontFamily: 'Pretendard',
                    fontSize: '15px',
                    fontStyle: 'normal',
                    fontWeight: 500,
                    lineHeight: '23px',
                    letterSpacing: '-0.6px'
                  }}>
                    {currentDate.format('YYYY년 MM월')}
                  </Typography>
                </label>
                <button style={{border: 'none', backgroundColor: 'transparent', alignSelf: 'self-end'}} onClick={nextMonth}>
                  <svg width="7" height="16" viewBox="0 0 7 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M1 14.5L6 8L1 1.5" stroke="#201E33" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                  </svg>
                </button>
              </Row>
              <Row sx={{gap: '5px', justifyContent: 'flex-end'}}>
                <button style={{border: 'none', backgroundColor: 'transparent'}} onClick={clickInsert}>
                  <Typography sx={{
                    color: '#7A7986',
                    textAlign: 'center',
                    fontFamily: 'Pretendard',
                    fontSize: '25px',
                    fontStyle: 'normal',
                    fontWeight: 500,
                    lineHeight: '23px',
                    letterSpacing: '-0.6px'
                  }}>
                    +
                  </Typography>
                </button>
                <button style={{border: 'none', backgroundColor: 'transparent'}} onClick={deleteItem}>
                  <Typography sx={{
                    color: '#7A7986',
                    textAlign: 'center',
                    fontFamily: 'Pretendard',
                    fontSize: '25px',
                    fontStyle: 'normal',
                    fontWeight: 500,
                    lineHeight: '23px',
                    letterSpacing: '-0.6px'
                  }}>
                    -
                  </Typography>
                </button>
                {/* <Button variant="contained" onClick={deleteItem}>삭제</Button> */}
                <button style={{border: 'none', backgroundColor: 'transparent', alignSelf: 'self-end'}} onClick={refresh}>
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" fill="none">
                    <circle cx="8" cy="8" r="7.5" stroke="#8B8E9B"/>
                    <mask id="path-2-inside-1_19840_23692" fill="white">
                      <path d="M12 8C12 8.88054 11.7095 9.73647 11.1734 10.435C10.6374 11.1336 9.88581 11.6358 9.03528 11.8637C8.18474 12.0916 7.28278 12.0325 6.46927 11.6955C5.65576 11.3586 4.97617 10.7626 4.5359 10C4.09563 9.23743 3.91929 8.3509 4.03422 7.4779C4.14915 6.60489 4.54894 5.79421 5.17157 5.17157C5.79421 4.54894 6.60489 4.14915 7.4779 4.03422C8.3509 3.91929 9.23743 4.09563 10 4.5359L9.4 5.57513C8.8662 5.26694 8.24563 5.1435 7.63453 5.22395C7.02342 5.30441 6.45594 5.58426 6.0201 6.0201C5.58426 6.45594 5.30441 7.02342 5.22395 7.63453C5.1435 8.24563 5.26694 8.8662 5.57513 9.4C5.88332 9.9338 6.35903 10.351 6.92849 10.5869C7.49794 10.8227 8.12932 10.8641 8.72469 10.7046C9.32007 10.5451 9.84616 10.1935 10.2214 9.70453C10.5966 9.21553 10.8 8.61638 10.8 8H12Z"/>
                    </mask>
                    <path d="M12 8C12 8.88054 11.7095 9.73647 11.1734 10.435C10.6374 11.1336 9.88581 11.6358 9.03528 11.8637C8.18474 12.0916 7.28278 12.0325 6.46927 11.6955C5.65576 11.3586 4.97617 10.7626 4.5359 10C4.09563 9.23743 3.91929 8.3509 4.03422 7.4779C4.14915 6.60489 4.54894 5.79421 5.17157 5.17157C5.79421 4.54894 6.60489 4.14915 7.4779 4.03422C8.3509 3.91929 9.23743 4.09563 10 4.5359L9.4 5.57513C8.8662 5.26694 8.24563 5.1435 7.63453 5.22395C7.02342 5.30441 6.45594 5.58426 6.0201 6.0201C5.58426 6.45594 5.30441 7.02342 5.22395 7.63453C5.1435 8.24563 5.26694 8.8662 5.57513 9.4C5.88332 9.9338 6.35903 10.351 6.92849 10.5869C7.49794 10.8227 8.12932 10.8641 8.72469 10.7046C9.32007 10.5451 9.84616 10.1935 10.2214 9.70453C10.5966 9.21553 10.8 8.61638 10.8 8H12Z" stroke="#8B8E9B" stroke-width="2" mask="url(#path-2-inside-1_19840_23692)"/>
                    <path d="M9.01073 3.48038L10.0693 5.31386L8.23581 6.37243" stroke="#8B8E9B" stroke-linecap="round"/>
                  </svg>
                </button>
                <button style={{border: 'none', backgroundColor: 'transparent'}} onClick={completeItem}>완료</button>
              </Row>
            </Row>
            <Row sx={{height: '30px', marginTop: '5px', gap: '10px'}}>
              <Select value={filterOption} onChange={(e) => setFilterOption(e.target.value as string)}>
                <MenuItem value="1">전체</MenuItem>
                <MenuItem value="2">입금</MenuItem>
                <MenuItem value="3">출금</MenuItem>
                <MenuItem value="4">공유</MenuItem>
              </Select>
              {/* <FormControlLabel
                label="삭제 포함"
                control={<Checkbox checked={includeDelete}
                onChange={() => setIncludeDelete(!includeDelete)}/>}
              /> */}
            </Row>
          </Col>
          <div style={{ height: 400, width: '100%' }}>
            <DataGrid
              loading={loading}
              rows={filterRows as any}
              columns={columns}
              editMode='row'
              initialState={{
                pagination: {paginationModel: { page: 0, pageSize: 5 }},
                columns: {columnVisibilityModel: {id: false, memo: true, _id: false, insuranceBack: false}}
              }}
              checkboxSelection
              disableColumnSorting
              disableColumnResize
              disableVirtualization
              disableColumnMenu
              onRowSelectionModelChange={(rowSelectionModel: GridRowSelectionModel) => {
                setSelectedRows(rowSelectionModel)
              }}
            />
          </div>
          <TotalPrice>
            <Row sx={{height: '30px', justifyContent: 'space-between'}}>
              <FormControlLabel
                label="공유 포함"
                sx={{width: '40%'}}
                control={<Checkbox checked={includeShare}
                onChange={() => setIncludeShare(!includeShare)}/>}
              />
              <Row style={{justifyContent: 'space-between', alignItems: 'center', width: '60%', marginLeft: '5px'}}>
                <Row style={{gap: '10px', color: 'red'}}>
                  <label>입</label>
                  <label>{inPrice.toLocaleString()}</label>
                </Row>
                <Row style={{gap: '10px', color: 'blue'}}>
                  <label>출</label>
                  <label>{outPrice.toLocaleString()}</label>
                </Row>
              </Row>
            </Row>
          </TotalPrice>
        </div>        
        {imageInfo && 
          <ImageViewer
            source={imageInfo.source as string}
            width={imageInfo.width}
            height={imageInfo.height}
            onClose={() => setImageInfo(undefined)}/>}
        {selectItem && <UsedPopup usedData={selectItem} onSave={cbSaveClick} onClose={cbCloseClick} />}
        <style jsx>{style}</style>
      </div>
    </>
  );
}

const TotalPrice = styled.div`
  padding: 5px;
  margin-top: 10px;

  font-size: 15px;
  border: 1px solid gray;
  border-radius: 5px;
`