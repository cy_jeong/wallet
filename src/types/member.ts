export interface  IMember {
  userId: string;
  password: string;
  name: string;
  register: Date;
  remove?: true | false;
}

export interface ISave {
  price: string;
  memo: string;
  image: string;
}

export interface ICards {
  _id: object;
  number: string;
  name: string;
  image: string;
}

export interface IImageInfo {
  source: string,
  width: number,
  height: number,
}

export interface IUsedData {
  _id?: string;
  id: number;
  share: boolean;
  inOut: boolean;
  price: string;
  memo: string;
  image?: IImageInfo;
  writer: string;
  userId: string;
  insuranceBack: string;
  complete?: boolean;
  deleted?: boolean;
  isNew?: boolean;
}