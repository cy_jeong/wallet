'use server'

import { mongoDB } from "@/src/util/mongodb";
import { isEmptyArray, isEmptyObject } from "@/src/util/function";
import { NextApiRequest, NextApiResponse } from "next";

export default async function Handler(
    request: NextApiRequest,
    response: NextApiResponse
) {
  if (request.method === 'GET') {
    const {userId, password} = request.query

    const collection = await mongoDB
    .then(mongo => mongo.db('wallet'))
    .then(mongo => mongo.collection('users'))
    .then(mongo => mongo.find({userId}))
    .then(mongo => mongo.toArray())

    const user = collection.find((item) => item.password === password)
    if (!isEmptyArray(collection) && !isEmptyObject(user as object))
      return response.status(200).send({error: false, user: user})
    else if (isEmptyArray(collection))
      return response.status(200).send({error: true, message: '존재 하지 않는 사용자입니다.'})
    else if (isEmptyObject(user as object))
      return response.status(200).send({error: true, message: '비밀번호를 확인 하세요.'})
    else
      return response.status(200).send({error: true, message: '존재 하지 않는 사용자입니다.'})
  }

  return response.status(400);
}