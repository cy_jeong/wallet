'use server'

import { isEmptyObject } from "@/src/util/function";
import { mongoDB } from "@/src/util/mongodb";
import { NextApiRequest, NextApiResponse } from "next";

export default async function Handler(
    request: NextApiRequest,
    response: NextApiResponse
) {
  if (request.method === 'GET') {
    const {fromDate, toDate, userId, includeShare} = request.query

    if (isEmptyObject(fromDate) || isEmptyObject(toDate) || isEmptyObject(userId) || isEmptyObject(includeShare)) {
      return response.status(400).send({
        error: true,
        message: '필수 입렵값 누락'
      });
    }

    const list = await mongoDB
    .then(mongo => mongo.db('wallet'))
    .then(mongo => mongo.collection('used'))
    .then(mongo => mongo.aggregate([
      {
        $match: {$and: [{id : {$gte : Number(fromDate), $lte : Number(toDate)}}, {$or: [{userId: userId}, {share: true}]}]}
      },
      {
        $group: {
          _id: {
            share: '$share',
            price: '$price',
            inout: '$inOut',
            insuranceBack: '$insuranceBack'
          }
        }
      }
    ]))
    .then(mongo => mongo.toArray())
    
    let inPrice: number = 0;
    let outPrice: number = 0;
    const share = String(includeShare).toLowerCase() === 'true';
    list.filter((item) => {
      if (share || !item._id.share) {
        if (item._id.inout) inPrice += Number(item._id.price)
        else outPrice += Number(item._id.price)

        inPrice += Number(item._id.insuranceBack)
      }
    })

    return response.status(200).send(
      {
        error: false,
        total: {inPrice, outPrice}
      }
    )
  }

  return response.status(400);
}

export const config = {
  api: {
    bodyParser: {
      sizeLimit:"10mb"
    }
  }
}