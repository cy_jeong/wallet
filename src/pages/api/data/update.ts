'use server'

import { mongoDB } from "@/src/util/mongodb";
import { NextApiRequest, NextApiResponse } from "next";

export default async function ApiHandler(
    request: NextApiRequest,
    response: NextApiResponse
) {
  if (request.method === 'POST') {
    const {id, complete, deleted} =  request.body

    const collection = await mongoDB
    .then(mongo => mongo.db('wallet'))
    .then(mongo => mongo.collection('used'))    

    let update;
    const filter = {id : {$in : id}}

    if (complete) update = {$set : {complete}}
    else if (deleted) update = {$set : {deleted}}
    else {
      response.status(200).json({error: true, message: 'empty is update item!'})
      return
    }

    const result = await collection.updateMany(filter, update, {upsert: true})

    // console.log('result : ', result)

    response.status(200).json({success: true, modifiedCount : result.modifiedCount, matchedCount: result.matchedCount})
  }
  
  return response.status(400);
}