'use server'

import { mongoDB } from "@/src/util/mongodb";
import { NextApiRequest, NextApiResponse } from "next";

export default async function Handler(
    request: NextApiRequest,
    response: NextApiResponse
) {
  if (request.method === 'GET') {
    const {fromDate, toDate, userId} = request.query

    const list = await mongoDB
    .then(mongo => mongo.db('wallet'))
    .then(mongo => mongo.collection('used'))
    .then(mongo => mongo.find(
      {
        $and: [
          {
            id : 
            {
              $gte : Number(fromDate),
              $lte : Number(toDate)}
          },
          {
            $or: [
              {
                userId: userId
              },
              {
                share: true
              }
            ]
          }
        ]
      }))
    .then(mongo => mongo.toArray())

    return response.status(200).send(
      {
        error: false,
        list: list.reverse()
      }
    )
  } else if (request.method === 'POST') {
    const {body} = request;
    try {
      const collection = await mongoDB
      .then(mongo => mongo.db('wallet'))
      .then(mongo => mongo.collection('used'))

      const filter = {_id: body._id}
      const update = {$set: {...body}}

      const result = await collection.updateOne(filter, update, {upsert: true})
      return response.status(200).send({success: true, data: {
        ...body, 
        _id: body.id.toString()
      }})
    } catch (ex) {
      console.log('upsert error : ', ex)
      return response.status(200).send({success: false})
    }
  }

  return response.status(400);
}

export const config = {
  api: {
    bodyParser: {
      sizeLimit:"10mb"
    }
  }
}